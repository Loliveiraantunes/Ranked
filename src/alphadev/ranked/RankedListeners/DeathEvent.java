package alphadev.ranked.RankedListeners;

import alphadev.ranked.Util.ConfigManager;
import alphadev.ranked.Util.RankedConfig;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathEvent {


    public  static void SetKill(PlayerDeathEvent event){

        ConfigManager configManager = ConfigManager.getInstance();
        RankedConfig config = RankedConfig.getInstance();
        Player player= event.getEntity().getKiller();

        String pathStatus = "Players."+player.getName()+".ranking.status";

        config.getConfig().set(pathStatus,config.getConfig().getInt("Players."+player.getName()+".ranking.status")+100);
        config.SaveConfig();

        configManager.setPrefixTeste(player);

    }


}
