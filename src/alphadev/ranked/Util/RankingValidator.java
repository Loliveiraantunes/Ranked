package alphadev.ranked.Util;

import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;


public class RankingValidator {

    private RankedConfig config = RankedConfig.getInstance();
    private static  RankingValidator rankingValidator = null;

    public String Validator(Player player){
        String path = "Players."+player.getName()+".ranking.status";
        int status = config.getConfig().getInt(path);
        return CalcRanking(status);
    }


    public String CalcRanking(int value) {
        List status = Arrays.asList(config.getFileConfigurationSettings().getConfigurationSection("Ranked.Status").getKeys(false).toArray());
        String elo = " ";
        for (int i = 0; i < status.size(); i++) {
            if (value >= config.getFileConfigurationSettings().getInt("Ranked.Status." + status.get(i))) {
                elo = (String) status.get(i);
            }
        }
        return elo;
    }

    public  static RankingValidator getInstance(){
        if(rankingValidator == null) rankingValidator = new RankingValidator();
        return  rankingValidator;
    }



}
