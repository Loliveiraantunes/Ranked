package alphadev.ranked.Util;

import alphadev.ranked.RankedListeners.DeathEvent;
import alphadev.ranked.RankedListeners.JoinEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class RegisterEvents implements Listener {



    @EventHandler
    public void onJoin(PlayerInteractEvent event){

        JoinEvent.joinInServer(event);

    }


    @EventHandler
    public void onDeath(PlayerDeathEvent event){
        DeathEvent.SetKill(event);
    }



}
