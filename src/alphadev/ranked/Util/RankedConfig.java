package alphadev.ranked.Util;

import alphadev.ranked.Main;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class RankedConfig {

    private File FolderConfig, config,settings;
    private Plugin plugin= Main.getPlugin(Main.class);
    private FileConfiguration fileConfiguration, fileConfigurationSettings;
    private static RankedConfig rankedConfig = null;
    private ConfigManager defaultConfig ;

    public void CreatingConfig(){

        FolderConfig = plugin.getDataFolder();

        config = new File(FolderConfig, "PlayersRanked.yml");
        settings = new File(FolderConfig, "SettingsRanked.yml");


        fileConfiguration = new YamlConfiguration();
        fileConfigurationSettings = new YamlConfiguration();

        if(!FolderConfig.exists()){
            try{
                FolderConfig.mkdir();
            }catch (Exception ex){
                plugin.getLogger().info("§c Cannot create the dir: "+ex.getMessage());
            }
        }

        if(!config.exists()){
            try {
                config.createNewFile();
            } catch (IOException ex) {
                plugin.getLogger().info("§c Cannot create the file: "+ex.getMessage());
            }

        }

        if(!settings.exists()){
            try {

                settings.createNewFile();

            } catch (IOException ex) {
                plugin.getLogger().info("§c Cannot create the file: "+ex.getMessage());
            }
            defaultConfig = ConfigManager.getInstance();
            defaultConfig.setDefaultSettings();
        }
        LoadFiles();
    }

    public   void  LoadFiles(){
        try {
            fileConfiguration.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        try {
            fileConfigurationSettings.load(settings);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
    public FileConfiguration getConfig(){
        return  fileConfiguration;
    }

    public FileConfiguration getFileConfigurationSettings(){
        return  fileConfigurationSettings;
    }

    public void SaveConfig()  {
        try {
            getConfig().save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void SaveSettings(){
        try {
            getFileConfigurationSettings().save(settings);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public  static RankedConfig getInstance(){
        if(rankedConfig == null) rankedConfig = new RankedConfig();
        return  rankedConfig;
    }




}
