package alphadev.ranked.Util;

import alphadev.ranked.RankedStatus.RankedStatus;
import org.bukkit.entity.Player;

public class ConfigManager {


    RankedConfig config = RankedConfig.getInstance();

    static ConfigManager configManager =null;

    public RankingValidator rankingValidator ;

        public  void  setDefaultSettings(){

            int bronze,prata,ouro,platina,diamante,mestre,deus;

            String path =  "Ranked.Status.";
            String prefix = "Ranked.Prefix.";
            String messages = "Ranked.messages.";

          bronze=RankedStatus.BRONZE.setPoints(400);
          prata=RankedStatus.PRATA.setPoints(700);
          ouro= RankedStatus.OURO.setPoints(1400);
          platina=RankedStatus.PLATINA.setPoints(2800);
          diamante=RankedStatus.DIAMANTE.setPoints(6000);
          mestre=RankedStatus.MESTRE.setPoints(9000);
          deus=RankedStatus.DEUS.setPoints(13000);

            config.getFileConfigurationSettings().set(path+"bronze",bronze);
            config.getFileConfigurationSettings().set(path+"prata",prata);
            config.getFileConfigurationSettings().set(path+"ouro",ouro);
            config.getFileConfigurationSettings().set(path+"platina",platina);
            config.getFileConfigurationSettings().set(path+"diamante",diamante);
            config.getFileConfigurationSettings().set(path+"mestre",mestre);
            config.getFileConfigurationSettings().set(path+"deus",deus);

            config.getFileConfigurationSettings().set(prefix+"bronze","§b[§eB§b]§f.");
            config.getFileConfigurationSettings().set(prefix+"prata","§b[§7P§b]§f.");
            config.getFileConfigurationSettings().set(prefix+"ouro","§b[§6O§b]§f.");
            config.getFileConfigurationSettings().set(prefix+"platina","§b[§5P§b]§f.");
            config.getFileConfigurationSettings().set(prefix+"diamante","§b[D]§f.");
            config.getFileConfigurationSettings().set(prefix+"mestre","§b[§8M§b]§f.");
            config.getFileConfigurationSettings().set(prefix+"deus","§b[§f§lGOD§b]§f.");

            config.getFileConfigurationSettings().set(messages+"promote","§5@§aVocê foi Promovido a §b§l");

            config.SaveSettings();

        }


        public void setPrefixTeste(Player player){

                rankingValidator = RankingValidator.getInstance();
                String messages = "Ranked.messages.";

                if(!config.getConfig().getString("Players."+player.getName()+".ranking.elo").equalsIgnoreCase( rankingValidator.Validator(player))){

                    if(!config.getConfig().getString("Players."+player.getName()+".ranking.elo").equalsIgnoreCase("unranked")){
                        player.sendMessage(config.getFileConfigurationSettings().getString(messages+"promote")+" "+rankingValidator.Validator(player).toUpperCase());

                        config.getConfig().set("Players."+player.getName()+".ranking.elo",rankingValidator.Validator(player));

                        String prefix = "Ranked.Prefix."+rankingValidator.Validator(player);
                        player.setDisplayName(config.getFileConfigurationSettings().getString(prefix)+player.getName());
                        player.setPlayerListName(config.getFileConfigurationSettings().getString(prefix)+player.getName());
                    }

                }

            config.SaveConfig();
        }



        public static ConfigManager getInstance(){
            if(configManager == null) configManager = new ConfigManager();
            return  configManager;
        }





}
