package alphadev.ranked.RankedStatus;

public enum RankedStatus {

    BRONZE {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    PRATA {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    OURO {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    PLATINA {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    DIAMANTE {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    MESTRE {
        @Override
        public int setPoints(int points) {

            return points;
        }
    },
    DEUS {
        @Override
        public int setPoints(int points) {

            return points;
        }
    };


    public abstract int setPoints(int points);


}
