package alphadev.ranked;

import alphadev.ranked.Util.MessageManager;
import alphadev.ranked.Util.RankedConfig;
import alphadev.ranked.Util.RegisterEvents;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {


    private PluginManager pm = this.getServer().getPluginManager();
    private RankedConfig rc;
    private MessageManager mm ;

    @Override
    public void onLoad() {

    mm = MessageManager.getInstance();
      mm.LoadMessage(this);

      rc = RankedConfig.getInstance();
       rc.CreatingConfig();

    }


    @Override
    public void onEnable() {
        pm.registerEvents(new RegisterEvents(), this);

    }


    @Override
    public void onDisable() {
        HandlerList.unregisterAll();
    }
}
